export class Rental {
  /**
   * @param {Movie} movie
   * @param {number} daysRented
   */
  constructor(movie, daysRented) {
    this.movie = movie;
    this.daysRented = daysRented;
  }
}
